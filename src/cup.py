from typing import Dict, List


class Cup:

    __ingredients: Dict[str, int] = {}

    def add_zero_key(self, keys: list):
        for key in keys:
            self.__ingredients[key] = 0

    def add(self, ingredient: str) -> None:
        if ingredient in self.__ingredients:
            self.__ingredients[ingredient] += 1
        else:
            self.__ingredients[ingredient] = 1

    def compose_response(self) -> dict:
        return self.__ingredients

    def set_zero_ingridients(self, zero_ingredients: List[str]) -> None:
        self.add_zero_key(zero_ingredients)
