from cup import Cup
from cup_factory import CupFactory
from recipe import Recipe


class AbstractMaker(Recipe):

    __cup_factory: CupFactory = CupFactory()

    def _take_new_empty_cup(self) -> Cup:
        return CupFactory().get_cup()

    def set_cup_factory(self, cup_factory: CupFactory):
        self.__cup_factory = cup_factory
