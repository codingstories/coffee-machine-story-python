from abstract_maker import AbstractMaker
from cup import Cup


class LatteMaker(AbstractMaker):

    def make(self) -> Cup:
        cup: Cup = self._take_new_empty_cup()
        cup.add("espresso")
        cup.add("milk")
        cup.add("milk")
        cup.add("foam")
        return cup
