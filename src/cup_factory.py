from cup import Cup


class CupFactory:

    def get_cup(self) -> Cup:
        cup: Cup = Cup()
        cup.set_zero_ingridients(["espresso", "water", "milk", "foam"])
        return cup